# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from functools import reduce
from django.http import HttpResponse, Http404
from django.utils.encoding import smart_str
from django.views.decorators.csrf import csrf_exempt

from wioframework import utilviews as uv
from wioframework import decorators as dec

from cloud.models import Node
from compute import models

import settings


import Pyro4


@csrf_exempt
@dec.permission_based
def result(request, oid):
    try:
        entry = models.ExecRequest.objects.get(id=str(oid))
    except:
        # print "no such requst id",str(job_id)
        raise Http404()

    # FIXME: SWITCH THIS CODE TO ASYNC
    if not request.user.is_superuser:
        if not entry.access_key == request.REQUEST["access_key"]:
            raise dec.PermissionException()

    proxy = Pyro4.Proxy(Node.objects.get(id=entry.metadata["buckethostid"]).metadata['ftd_uri'])
    proxy._pyroHmacKey = settings.HMACKEY
    filename = request.REQUEST["filename"]
    file_data = proxy.block_io(entry.metadata["bucketid"], filename, 0, "read")

    response = HttpResponse(file_data, content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(request.REQUEST["filename"])
    #response['X-Sendfile'] = smart_str(path_to_file)
    return response

result.default_url = 'result/([^/]+)'

VIEWS = reduce(lambda x, y: x + uv.AUDLV(y), models.MODELS, []) + [result]
