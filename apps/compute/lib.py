# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

import datetime
import json

NOSQL = False


def create_job_request(
        request,
        request_type="execute",
        memory=128,
        return_object=False,
        **kwargs):
    if not NOSQL:
        from compute.models import Job
        newjob = {
            "owner": (request.user if request else None),
            "request_type": request_type,  # < what do we want to do
            #"served_by_algo_servers": False, # < means that this job must replied by "instances" that work as server
            "status": "SCHEDULED",  # < what is the status of the request
            "maturity": 0,
            #< maturity increase with time and maturity_rate it is way to decide who has to be processed first
            "maturity_rate": 10,
            "safe_env": False,
            # in safe_env the process is chroot and all the network
            # communications are stopped (TODO: IMPLEMENT)
            # < This is the host on which the computations are done
            "host": None,
            #"owned_by": "userid",  # < This is the user that will pay in the end
            # "model": {"id": None, "parameters": {}},  # < The program / algorithm / model that we must run
            #"model__id": None,
            #"model__parameters": {},
            #"input_datasets": [{}],  # < Datasets that may have to be connected to the runtime environment
            # "ulimits": {  # < LIMITS ON THESE SPECIFIC REQUEST (TODO: Implement)
            #             "data": memory * 1024,
            #             "memory": memory * 1024,
            #             "cputime": 48 * 3600,
            #             "processes": 4,
            # },
            "ulimits": {'data': memory * 1024, "memory": memory * 1024, "cputime": 48 * 3600, "processes": 4},
            #"metadata__errors": [],
            #"parent_request_id": None, ## < used in map / multithreading
            # < for obvious accounting / stats
            "created_at": datetime.datetime.utcnow(),
            "started_run_at": None,  # < for obvious accounting / stats
            "finished_run_at": None,  # < for obvious accounting / stats
            #"estimated_duration":
        }

        newjob.update(**kwargs)
        print newjob        
        
        nj = Job()
        for i in newjob.items():
            setattr(nj,i[0],i[1])
        print nj.__dict__
        nj.save()
        if return_object:
            return nj
    else:
        newjob = {
            "owner__id": (request.user.id if request else None),
            #  "owner": {"$id$": request.user.id},  # bson.objectid.ObjectId(request.user.id),
            "type": request_type,  # < what do we want to do
            # < means that this job must replied by "instances" that work as server
            "served_by_algo_servers": False,
            "status": "SCHEDULED",  # < what is the status of the request
            "maturity": 0,
            #< maturity increase with time and maturity_rate it is way to decide who has to be processed first
            "maturity_rate": 10,
            "safe_env": False,
            # in safe_env the process is chroot and all the network
            # communications are stopped (TODO: IMPLEMENT)
            # < This is the host on which the computation are done
            "host": None,
            #"owned_by": "userid",  # < This is the user that will pay in the end
            # "model": {"id": None, "parameters": {}},  # < The program / algorithm / model that we must run
            "model__id": None,
            "model__parameters": {},
            #"input_datasets": [{}],  # < Datasets that may have to be connected to the runtime environment
            # "ulimits": {  # < LIMITS ON THESE SPECIFIC REQUEST (TODO: Implement)
            #             "data": memory * 1024,
            #             "memory": memory * 1024,
            #             "cputime": 48 * 3600,
            #             "processes": 4,
            # },
            "ulimits__data": memory * 1024,
            "ulimits__memory": memory * 1024,
            "ulimits__cputime": 48 * 3600,
            "ulimits__processes": 4,

            #"metadata__errors": [],
            "parent_request_id": None,  # < used in map / multithreading
            # < for obvious accounting / stats
            "created_at": datetime.datetime.utcnow(),
            "started_run_at": None,  # < for obvious accounting / stats
            "finished_run_at": None,  # < for obvious accounting / stats
            #"estimated_duration":
        }

        # errors : JsonField

        # ADD ANY FIELD THAT WE MAY HAVE FORGOTTEN AND STAY AGILE
        newjob.update(**kwargs)

        # PUT IT IN THE JOB QUEUE
        # return "-1" # FIXME: CREATED
        print newjob
        x = mongo_db.compute_job.insert(newjob)

        if return_object:
            return x, newjob
        else:
            return x


def create_job(request, *xargs, **kwargs):
    return create_job_request(
        request,
        request_type="execute",
        memory=768,
        command=json.dumps(xargs),
        **kwargs)


def create_privileged_job(request, *xargs, **kwargs):
    return create_job_request(
        request,
        request_type="execute",
        memory=768,
        command=json.dumps(xargs),
        privileged=True,
        **kwargs)




if __name__ == "__main__":
    # """ Used for testing """
    import sys,os,subprocess    
    os.environ["DJANGO_SQLITE3_AUTOCOMMIT_BUGFIX"] = "0"
    os.environ["DJANGO_SETTINGS_MODULE"] = 'aiosciweb1.settings'
    import aiosciweb1.settings
    
    from django.apps.registry import apps
    apps.populate(aiosciweb1.settings.INSTALLED_APPS)
    print "CREATING JOB"  
    from software.models import Package
    create_job(None, "./exe", package=Package.objects.filter(wiostate='V')[0])
