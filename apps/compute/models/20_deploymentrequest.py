#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket


from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr

from bson.objectid import ObjectId
from django.views.decorators.csrf import csrf_exempt

from django.forms import widgets as bwidgets
from wioframework import widgets as wwidgets


@wideio_owned()
@wideio_timestamped
@wideiomodel
class DeploymentRequest(models.Model):

    """
    This class is not yet implemented.
    """
    algorithm = models.ForeignKey('science.Algorithm', blank=True, null=True, related_name="current_deployments",
                                  help_text="A link to the algorithm that you want to run")
    parameters = models.TextField(null=True, blank=True, help_text="Parameters that are used to configure the node")
    session = models.ForeignKey('AlgorithmSession', null=True, blank=True, default=None)

    # the public key will simply be id under base64
    # users will need this key to prove that they can access it
    secret_access_key = models.CharField(max_length=64)

    def get_usage(self, from_time=None, to_time=None):
        from compute.models import Job

        if to_time is None or to_time > datetime.datetime.now():
            to_time = datetime.datetime.now()
        if from_time is None or from_time > datetime.datetime.now():
            from_time = datetime.datetime(to_time.year, to_time.month, to_time.day, 0, 0, 0, 0)

        jobs = []

        for e in mongo_db.compute_job.find(
            {"$and": [
                {"deployment_order": ObjectId(self.id)},
                {"type": "runserver"},
                {"owner": ObjectId(self.owner.id)}
            ]}
        ):
            jobs.append(e)

        bill_start = None
        bill_end = None

        from accounts.models import UserAccount
        result = {"user": UserAccount.objects.get(id=ObjectId(self.owner.id))}
        total_duration = None
        orders = []

        for j in jobs:
            if j["started_run_at"] is None:
                bill_start = from_time
            else:
                if j["started_run_at"] > from_time:
                    bill_start = j["started_run_at"]
                else:
                    bill_start = from_time

            if j["finished_run_at"] is None:
                bill_end = datetime.datetime.now()
            else:
                if j["finished_run_at"] > to_time:
                    bill_end = to_time
                else:
                    bill_end = j["finished_run_at"]

            order = {}
            order["server"] = Job.objects.get(id=j["_id"])
            order["bill_start"] = bill_start
            order["bill_end"] = bill_end
            order["duration"] = bill_end - bill_start

            # reqs = mongo_db.compute_job.find({"runserver": })
            # platform agnostic way:
            reqs = Job.objects.filter(runserver=ObjectId(j["_id"]))

            requests = []
            total_requests = 0
            completed_requests = 0

            for e in reqs:
                total_requests += 1
                req = {'request': Job.objects.get(id=e["_id"])}
                if e["finished_run_at"]:
                    req["duration"] = e[
                        "finished_run_at"] - e["started_run_at"]
                else:
                    req["duration"] = bill_end - e["started_run_at"]

                req["duration"] = req["duration"] - \
                    datetime.timedelta(0, 0, req["duration"].microseconds)
                requests.append(req)
                if e["status"] == "DONE":
                    completed_requests += 1

            order["requests"] = requests
            order["total_requests"] = total_requests
            order["completed_requests"] = completed_requests

            if total_duration is None:
                total_duration = order["duration"]
            else:
                total_duration += order["duration"]

            order["duration"] = order["duration"] - \
                datetime.timedelta(0, 0, order["duration"].microseconds)
            orders.append(order)

        result["orders"] = orders
        if total_duration is not None:
            # for displaying without microseconds
            total_duration = total_duration - \
                datetime.timedelta(0, 0, total_duration.microseconds)
        result["total_duration"] = total_duration
        result["total_cost"] = total_duration.total_seconds() / 10
        return result

    def get_publickey(self):
        import base64
        return base64.b64encode(self.id)

    def get_secretkey(self):
        return self.secret_access_key

    def on_add(self, request):
        # FIXME: RANDOM WOULD BE BETTER THERE
        self.secret_access_key = str(uuid.uuid1())
        self.save()

    def on_view(self, request):
        usage = self.get_usage(datetime.datetime(2013, 1, 1, 0, 0, 0))

        return {
            'usage': usage,
        }

    @wideio_relaction(
        'compute.ScientificModel',
        "Deploy this algorithm on the cloud",
        mimetype="text/html")
    def run_a_query(self, possible=lambda u, r: (u.owner == r.user)):
        return HttpResponseRedirect(DeploymentRequest.get_add_url())

    class WIDEIO_Meta:
        icon = "icon-cloud-download"
        permissions = dec.perm_for_logged_users_only
        form_exclude = ['secret_access_key']
        MOCKS = {
            'default':[
                {
                'algorithm': 'algorithm-0001',
                'parameters': '{}',
                'session': 'algorithm-session-0001',
                'secret_access_key': 'lfdskflasdjfklsad'
                }
            ]
        }
        class Actions:

            @wideio_action(
                "renew secret key",
                possible=lambda self,
                request: True,
                icon="icon-cogs")
            def renew_secret_key(self, request):
                self.secret_access_key = str(
                    uuid.uuid1())  # FIXME: RANDOM WOULD BE BETTER THERE
                self.save()
                return "alert('ok');"

            @wideio_action(
                "startserver",
                possible=lambda self,
                request: True,
                icon="icon-play-circle")
            def start_1_server(self, request):
                from compute.lib import create_job_request
                from cloud import PlatformType
                kwargs = {
                    'privileged': False,
                    'deployment_order': ObjectId(self.id),
                    'package': ObjectId(self.algorithm.package.id),
                    'session': ObjectId(self.session.id),
                    'protocol': 'json',
                    'return_object': True,
                }

                x, newjob = create_job_request(
                    request, "runserver", memory=768, **kwargs)
                return "alert('ok');"
