#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket


from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr

#from wioframework.mongo import mongo_db

from bson.objectid import ObjectId
from django.views.decorators.csrf import csrf_exempt

from django.forms import widgets as bwidgets
from wioframework import widgets as wwidgets
from wioframework import validators


@wideio_owned("owned", public="public")
@wideio_timestamped
@wideiomodel
class Terminal(models.Model):

    """
    A terminal is a saved environment that can be accessed to run programs/commands in.
    """
    name = models.CharField(
        max_length=32,
        db_index=True,
        db_validators=[
            validators.NameValidator(
                with_underscore=True)])
    # base_distribution=models.ForeignKey('BaseDistribution')
    # packages=models.ManyToManyField('software.PackageVersion')
    # public=models.BooleanField(default=False)
    environment = models.ForeignKey('compute.Environment')
    public = models.BooleanField(default=False)

    class WIDEIO_Meta:
        icon = "icon-terminal"
#    mandatory_fields=["environmnet"]
        add_button_text = "Run terminal"
        widgets = {
            #'packages':(lambda f:bwidgets.SelectMultiple)
            #'environment':(lambda f:wwidgets.SelectOrAdd(model=f.rel.get_related_field().model))
        }
        MOCKS={
            'default':[
                {
                    'name':'xterm-linux-0000',
                    'environment': 'environment-0000',
                    'public': False
                }
            ]
        }