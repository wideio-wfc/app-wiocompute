#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-

import os
import datetime
#from django.db import models
import wioframework.fields as models
from django.core.urlresolvers import reverse

from django.contrib.auth.models import AbstractUser  # , User
import zipfile
import settings
import json

from accounts.models import UserAccount

from wioframework.amodels import wideiomodel, wideio_owned, get_dependent_models, wideio_timestamped, wideio_action
from wioframework import decorators as dec

from wioframework.fields import JSONField


@wideio_timestamped
@wideio_owned(personal=1)
@wideiomodel
class Job(models.Model):
    """
    An ExecRequest consists in one or many Job.
    Job shall not exist without an ExecRequest exept if they are internal to the cloud

    This is an internal job to the cloud
      - some of these job may be billed;
      - some may not be billed;
      - some may have associated filed - some maynod
    """
    # IF THIS JOB IS RELATED TO A REQUEST OBJECT HERE IS THE REQUEST TO WHICH
    # THE JOB MAY ACCESS
    nrequestid = models.ForeignKey(
        'compute.ExecRequest',
        blank=True,
        null=True)
    hostname = models.CharField(
        blank=True,
        null=True,
        max_length=38)  # FIXME: could be provided through workerid
    workerid = models.CharField(blank=True, null=True, max_length=38)

    # MINIMAL INFORMATION REQUIRED BY THE SCHEDULER
    request_type = models.TextField()

    # served_by_algo_servers=ListField(models.TextField(blank=True,null=True))
    # #< means that this job must replied by "instances" that work as server
    status = models.CharField(default="SCHEDULED", max_length=16)
    parent = models.ForeignKey('Job', blank=True, null=True, related_name='children')

    # USER FOR DEBUGGING
    host = models.ForeignKey('cloud.Node', null=True)

    # exec request -> base distribution
    platform = models.CharField(max_length=256)
    
    package= models.ForeignKey(
        'software.Package',
        blank=True,
        null=True) #< The single package that we want to rent
        
    environment = models.ForeignKey(
        'compute.Environment',
        blank=True,
        null=True) #< A more complex environment to set up

    disable_redirects = models.BooleanField(default=False)

    trace = models.TextField(blank=True, null=True)
    stderr = models.TextField(blank=True, null=True)
    stdout = models.TextField(blank=True, null=True)

    query = JSONField()    # which algo,  which pkg, which session, etc...
    ulimits = JSONField()  # which config
    inputs = JSONField()
    outputs = JSONField()
    parameters = JSONField()

    # WHAT NEEDS TO RUN
    # type=models.TextField(blank=True,null=True) #< DEPRECATED : REQUEST _TYPE
    command = models.TextField(blank=True, null=True)
    privileged = models.BooleanField(default=False)

    # POLICY FOR SCHEDULING
    maturity = models.IntegerField()
    maturity_rate = models.IntegerField()

    # errors=ListField(models.TextField(blank=True,null=True,))
    deployment_order = models.ForeignKey(
        'compute.DeploymentRequest',
        null=True,
        editable=False)
    runserver = models.ForeignKey(
        'Job',
        blank=True,
        null=True,
        related_name='compute_jobs')

    # command=ListField(models.TextField(blank=True,null=True))
    on_complete = models.TextField(blank=True, null=True)
    safe_env = models.BooleanField(default=False)

    started_run_at = models.DateTimeField(null=True, blank=True)
    finished_run_at = models.DateTimeField(null=True, blank=True)

    def duration(self):
        if (self.finished_run_at):
            return self.finished_run_at - self.created_at
        else:
            return "Unfinished"

    def GetRequestFiles(self):
        try:
            return os.listdir(
                "/wideio/requests/" +
                self.nrequestid_id)  # < FIXME (use nrequesid)
        except Exception as e:
            return str(e)

    def can_view(self, request):
        return request.user.is_staff or self.owner == request.user

    def __unicode__(self):
        return "Job " + str(self.id) + " " + self.status + \
            "  [" + str(self.created_at) + "]"

    #class Meta:
    #    db_table = "queries"

    class WIDEIO_Meta:
        icon = "icon-cog"
        sort_default = {"created_at": -1}
        sort_enabled = ["created_at", "request_type", "started_at"]
        permissions = dec.perm_for_admin_only
        views_kwargs = {"list_add_enabled": False}

        class Actions:

            @wideio_action(
                "Kill",
                possible=lambda self,
                request: self.status in [
                    "processing",
                    "loading"],
                icon="icon-remove-sign")
            def kill(self, request):
                self.status = "KILLW"  # WAITING FOR KILL REQUEST
                self.save()

            #@wideio_action("Ping",possible=lambda self,request:self.status=="processing" and self.request_type="runserver" , icon="icon-remove-sign"):
            # def ping(self,request):
            #  ## ask to manually to update last_ping
            #  pass
            # should be a rel_action on runserver
