#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket


from django.core.urlresolvers import reverse

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from science.models import Algorithm

from wioframework.atomic_ops import atomic_incr

from bson.objectid import ObjectId
from django.views.decorators.csrf import csrf_exempt

from django.forms import widgets as bwidgets
from wioframework import widgets as wwidgets
from wioframework import validators


@wideio_timestamped
@wideio_owned()
#@wideio_buyable()
@wideiomodel
class ScientificModel(models.Model):

    """
    A scientific model publication entry represents the combination of an algorithm (as code) with some numerical data.


    It is a fact that a scientist has decided to "publish" a model on the platform.

    Essential metadata about the published algorithm.
    """
    ctr_name_s = "scientific model"
    ctr_name_p = "scientific models"

    name = models.CharField(max_length=64,
                            verbose_name="Name",
                            help_text="""
                                      Give a good informative name.
                                      """,
                            db_index=True,
                            db_validators=[validators.NameValidator(with_underscore=True)]
                            )
    #~ scientificmodel=models.ForeignKey(Algorithm, verbose_name ="Model")
    algorithm = models.ForeignKey( 'science.Algorithm', verbose_name="Algorithms")

    business_description = models.TextField(help_text="""
                                                    A small paragraph to explain what your published algorithm is about.

                                                    Example:
                                                    '''
                                                    This algorithm is able to recognise human faces
                                                    at different angles and in varied illumination.
                                                    It is provided with its training data.
                                                    '''
                                                    """

                                           )
    asset_right_holder_confirmed = models.BooleanField(default=False)  # must be True (formal form)

    limit_free_use = models.IntegerField(default=0, blank=True)  # < maximum free query per user per year
    credits_per_scientific_use = models.IntegerField(default=0, blank=True)  # < query cost for scientific
    limit_scientific_use = models.IntegerField(default=0, blank=True)
    credits_per_scientific_business_use = models.IntegerField( default=0, blank=True)  # < query cost for businesses
    limit_business_use = models.IntegerField(default=0, blank=True, help_text="0 means no limit of usage.")
    # input_based_pricing_function=EmbeddedModelField(FuncionRef)

    type_of_publication = models.IntegerField(
        default=0,
        choices=[
            (0, 'ALGORITHM_ONLY'),
            (1, 'TRAINED_ALGORITHM'),
            (2, 'ALGORITHM_FACTORY')],
        help_text=""" algorithm only= content of the algorithm package (no training data provided), trained algorithm = algorithm + training data, algorithm factory = untrained scientific model generating a new scientific model once trained """)  # < From ENUM : ALGO_ONLY|ALGO_PLUS_TRAINING_DATA

    def __unicode__(self):
        return self.name

    class WIDEIO_Meta:
        form_exclude = ['price']
        icon = "icon-cloud-upload"

        permissions = dec.perm_for_logged_users_only

        @wideio_relaction(Algorithm,
                          "Publish",
                          mimetype="text/html",
                          icon="icon-bullhorn",
                          possible=lambda u,
                          r: (u.owner == r.user))
        def publish(self, request):
            return HttpResponseRedirect(ScientificModel.get_add_url() +"?algorithm=" +self.id +"&name=" +self.name)
