#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket


from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr

#from wioframework.mongo import mongo_db

from bson.objectid import ObjectId
from django.views.decorators.csrf import csrf_exempt

from django.forms import widgets as bwidgets
from wioframework import widgets as wwidgets

##


@wideio_owned()
@wideio_timestamped
@wideiomodel
class BusinessRunmodelSubscription(models.Model):

    """
    This is the converse of a publication.
    Once a scientific team has published a model, businesses can subscribe to it.
    """
    subscribe_from = models.DateTimeField()
    subscribe_to = models.ForeignKey('ScientificModel')

    class WIDEIO_Meta:
        permissions = dec.perm_for_admin_only


@wideiomodel
class BusinessConsumed(models.Model):
    report = models.TextField()

    def __unicode__(self):
        return self.report

    class WIDEIO_Meta:
        permissions = dec.perm_for_admin_only


# suggestion Can't inherit from more than one table.
# Possible solution is abstract classes if one of the tables is only used for this model.
# for direct access by customers
#@wideiomodel
# class BusinessEmittedVouchers(models.Model):
    # DEPRECATED ~ CF . TOKEN CLASS
    # voucherid=models.CharField(max_length=128)
    # security_key=models.TextField()
    # ref_business_account=models.ForeignKey(UserAccount)
    # algos_authorized=ListField(models.ForeignKey('ComputeCloudNodeModelInstall'))
    # expirytime=models.DateTimeField()
    # amount_credited=EmbeddedModelField('MoneyAmount')
    # amount_remaining=EmbeddedModelField('MoneyAmount')

    # def __unicode__(self):
        # return self.voucherid
# MODELS.append(BusinessEmittedVouchers)
