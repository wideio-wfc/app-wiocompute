#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket
import Pyro4

from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr


from django.views.decorators.csrf import csrf_exempt

from django.forms import widgets as bwidgets
from wioframework import widgets as wwidgets
#from wioframework.zmq import get_zmq_context

from django.db import transaction
from wioframework.fields import JSONField


@wideio_owned("ordered_by")
@wideio_timestamped_autoexpiring(
    expire=datetime.timedelta(
        days=2),
    delete_database_entry_on_expire=True)  
@wideiomodel
class SchedReqRep(models.Model):
    """
    Arbitrary request send to scheduler with potential results from scheduler.
    
    Can only be manipulated by admin.
    """
    target_compute_node=models.ForeignKey('cloud.Node')
    request= JSONField()
    result = JSONField(null=True, blank=True)
    #webhook = models.URLField(null=True, blank=True) # < TODO CHANGE TO REFERENCE TO WEBHOOKS OBJECT
    access_key = models.TextField(null=True, blank=True)

    def __init__(self,*args,**kwargs):
        super(SchedReqRep,self).__init__(*args,**kwargs)
        self.sent=False
    
    def get_worker_id(self):
        if "wid" in self.result:
            return self.result["wid"]
        
        for w in self.target_compute_node.workers.all():
            if w.metadata:
              if "request" in w.metadata:
                if "schedreqrep_id" in w.metadata["request"]:
                  if w.metadata["request"]["schedreqrep_id"]==self.id:
                    return w.wid
                
    def get_worker(self):
        if "wid" in self.result:
            return self._get_worker(self.result["wid"])
        
        for w in self.target_compute_node.workers.all():
          if w.metadata:
            if "request" in w.metadata:
              if "schedreqrep_id" in w.metadata["request"]:
                if w.metadata["request"]["schedreqrep_id"]==self.id:            
                    return w                

    def _get_worker(self,wid=None):
        from cloud.models import Worker
        return Worker.objects.get(node=self.target_compute_node,wid=str(wid if wid!=None else self.get_worker_id()))

    
    def send(self):
            """
            SEND A SPECIFIC REQUEST TO A NODE/WORKER
            """
            if self.sent:
                return
            
            if not self.access_key:
              self.access_key = str(uuid.uuid4())

            #if (self.webhook):
            #    fields_update["on_complete"] = {"webhook": self.webhook}
            #    job.save()
            
            
            assert(False)
            # FIXME: UPDATE TO NEW MESSAGING
            from wioframework.serializers.json import StartYnJSONEncoder
            zmq_context = get_zmq_context()
            port = 10000
            host = self.target_compute_node.hostip
            zsocket = zmq_context.socket(zmq.REQ)
            zsocket.setsockopt(zmq.SNDTIMEO, 500)
            zsocket.setsockopt(zmq.RCVTIMEO, 500)
            zsocket.connect("tcp://%s:%s" % (host, port,))
            req=dict(self.request)
            req["schedreqrep_id"]=self.id
            self.save()
            done=False; retries=4
            while not done:
              try:
                zsocket.send(json.dumps(
                  req, cls=StartYnJSONEncoder))
                done=True
              except Exception,e:
                print "EXCEPTION DURING ZMQ SEND:",e
                time.sleep(0.005)
                retries-=1
                if retries<0:
                    raise
            done=False; retries=4
            while not done:
              try:
                res=zsocket.recv()
                done=True
              except:
                time.sleep(0.005)
                retries-=1
                if retries<0:
                    raise  
            if done:
              self.sent=True
            zsocket.close()
            return res
        
    
    def on_add(self, request):
        try:
            if request:
              self.ordered_by=request.user
              self.metadata["username"]=request.user.username
              self.metadata["apikey"]=request.user.get_extended_profile().api_key
            else:
              user=self.ordered_by
              if self.metadata==None:
                  self.metadata={}
              self.metadata["username"]=user.username
              self.metadata["apikey"]=user.get_extended_profile().api_key              
            if "metadata" not in self.request:
                self.request["metadata"]=self.metadata
            print "ONADD"
            self.send()
            #output.update({"access_key": self.access_key})
            #return output
        except Exception as ce:
            import backoffice.lib as debug
            debug.log_error(str(ce))
            import traceback
            import sys
            sys.stderr.write(
                "\n".join(map(str, traceback.format_tb(sys.exc_info()[2]))))
            print "EXCEPTION", ce
            raise
            return {'err': str(ce)}

    def on_view(self,request):
        if self.result!=None and request.REQUEST.get("_VIEW","view")=="view":
            if "wid" in self.result:
               w=self.get_worker()
               if (w.metadata!=None) and (w.metadata["request"]["schedreqrep_id"]==self.id):
                 return {'_redirect':"/direct/package-examples/terminal_applet/?sshhost=kappa.wide.io&sshuser=root&sshport="+str(self.result['port'])+"&schedreqrep_id="+str(self.id)}
            return {'_redirect':"/compute/schedreqrep/view/"+self.id+"/?_VIEW=down"}
            
    def can_view(self, request):
        return request.user.is_superuser or request.user == self.ordered_by

    class WIDEIO_Meta:
        API_EXPORT_ALL = True
        NO_DRAFT = True
        icon = "icon-flag-alt"
        form_exclude = [
            'access_key',
            ]
        # form_exclude += ['results','ipaddress']
        permissions = dec.perm_for_logged_users_only
        audlv_xargs = {
            'view_json_view': lambda x: dict(
                filter(
                    lambda i: i[0][0] != '_',
                    x.__dict__.items()))}
        audlv_xargs.update({'add_decorators': [csrf_exempt]})
        audlv_xargs.update({'view_decorators': [csrf_exempt]})
