#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import zipfile

from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

import  settings
from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr
from wioframework import validators
from science.models import Algorithm


@wideio_owned()
@wideiomodel
class AlgorithmSession(models.Model):

    """
    A session is a set of trained data associated with a specific algorithm.
    Algorithm do not have to have a session associated to them.

    - Most machine learning models tend to make sense only when they are associated
    with a session.

    - Some evalutionary algorithms, or complex system algorithm will use session to
    store their data.

    """
    ctr_name_s = "session"
    ctr_name_p = "sessions"

    name = models.CharField(
        max_length=64,
        verbose_name="Name",
        blank=True,
        null=True,
        db_index=True,
        db_validators=[
            validators.NameValidator(
                with_underscore=True)])
    algorithm = models.ForeignKey(
            Algorithm,
            verbose_name="Model",
            blank=True,
            null=True
        )

    description = models.TextField(max_length=30000, default="")
    source_zipfile = models.ForeignKey('extfields.File', verbose_name="ZIP file", blank=True, null=True)  # NEEDS VALIDATION AS ZIP

    def __unicode__(self):
        return self.name

    def on_add(self, request):
        self.save()
        os.mkdir("/wideio/sessions/" + str(self.id) + "/")

    def do_install(self, request, **kwargs):
        from compute.lib import create_privileged_job
        result = create_privileged_job(
            request,
            "wideio_install_session",
            self.id,
            **kwargs)
        return result

    def can_view(self, request):
        return request.user.is_superuser or request.user == self.ordered_by

    def on_view(self, request):  # FIXME: check permissions
        return {"files": os.listdir("/wideio/sessions/" + self.id)}

    class WIDEIO_Meta:
        icon = "icon-hdd"
        permissions = dec.perm_for_logged_users_only
        audlv_xargs = {'view_json_view': lambda x: dict(filter(lambda i: i[0][0] != '_', x.__dict__.items()))}

        MOCK= {
            'default':[
                {
                'name':'session-0000',
                'algorithm':'algorithm-0000'
                }
            ]
        }

        class Actions:
            @wideio_action("unzip", possible=lambda s, r: (s.source_zipfile and r.user.is_staff))
            def unzip(self, request):
                import subprocess
                # always the original zip (replaces)
                z = os.path.join("/wideio/webdata", str(self.source_zipfile))
                if zipfile.is_zipfile(z):
                    subprocess.Popen(["unzip", z], cwd="/wideio/sessions/" + self.id)
