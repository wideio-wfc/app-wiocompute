#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket
import Pyro4

from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr

# from wioframework.mongo import mongo_db, ObjectId

# try:
#  from bson.objectid import ObjectId


from django.views.decorators.csrf import csrf_exempt

from django.forms import widgets as bwidgets
from wioframework import widgets as wwidgets
#from wioframework.zmq import get_zmq_context

from django.db import transaction
STATUS_NOT_INITIALIZED = 0
STATUS_QUERY_SENT = 1
STATUS_QUERY_REPLIED = 2

DEPLOYMENT_ORDERS_ENABLED = False


def ExecRequest_util_content_save(i, fn):
    import hashlib
    import datetime
    import random
    i.job_id = hashlib.md5(
        str(random.random()) + str(datetime.datetime.now())).hexdigest()
    return str(i.job_id)


@wideio_owned("ordered_by")
@wideio_timestamped_autoexpiring(
    expire=datetime.timedelta(
        days=2),
    delete_database_entry_on_expire=False)  # FIXME: Is 2 days ok ??
#@wideio_buyable()
@wideiomodel
class ExecRequest(models.Model):

    """
    Request sent by a final user to run an algorithm on specific content.

    Additional options may be be passed to the models
    """
    job = models.ForeignKey('compute.Job', null=True)

    #~ model = models.ForeignKey(Algorithm,blank=True,null=True,related_name="current_requests")   #####TODO: REMOVE BLANK AND NULL(TESTING)
    algorithm = models.ForeignKey(
        'science.Algorithm',
        blank=True,
        null=True,
        related_name="current_requests",
        help_text="A link to the algorithm that you want to run")  # TODO: REMOVE BLANK AND NULL(TESTING)
    token = models.ForeignKey('Token', blank=True, null=True)

    inputs = models.TextField(
        null=True,
        blank=True,
        help_text="Inputs of the algorithm component in this request")  # """ parameters of the nodes in dflow """
    parameters = models.TextField(
        null=True,
        blank=True,
        help_text="Parameters that are used to configure the algorithm component for one or many request")  # """ parameters of the nodes in dflow """

    status = models.IntegerField(default=STATUS_NOT_INITIALIZED,
                                 choices=((0, STATUS_NOT_INITIALIZED),
                                          (1, STATUS_QUERY_SENT),
                                          (2, STATUS_QUERY_REPLIED)
                                          )
                                 )  # """ request send ... ? """
    # ipaddress=models.IPAddressField(null=True,blank=True)
    result = models.TextField(null=True, blank=True)
    webhook = models.URLField(null=True, blank=True)

    access_key = models.TextField(null=True, blank=True)
    associated_files = models.ManyToManyField(
        'extfields.File',
        null=True,
        blank=True,
        help_text="Identifiers for files that have to be passed as input")

    use_server = models.ForeignKey('DeploymentRequest', null=True)

    session = models.ForeignKey(
        'AlgorithmSession',
        null=True,
        blank=True,
        default=None)

    batch_query = models.BooleanField(default=False)
    dataset_expression = models.TextField(
        null=True,
        blank=True,
        help_text="Dataset expression that is of main interest")
    training_dataset_expression = models.TextField(
        null=True,
        blank=True,
        help_text="If your model requires training, you have to specify this field")
#     might be worth adding
#     prep_dataset_expression = models.TextField(null = True, blank = True, help_text = "Dataset expression launched in the beginning. Can be used for caching some datasets")


    def on_add(self, request):
        # WE ARE NOT USING S3QL ANYMORE - still checking the state of the system may be good
        # if not self.testS3FS():
        #    from backoffice.lib import log_error
        #    log_error("S3FS is down - cannot perform request")
        #    return {'err':'S3FS is down'}
        output = {}
        try:
            ###
            # OK FIRST QUESTION FIRST : WHO IS ASKING ?
            ###
            print "upload handlers on add:", request.upload_handlers
            from termcolor import cprint
            cprint(request, "blue")
            if request.user.id is not None:
                self.ordered_by = request.user
            elif "token_" in request.REQUEST:
                try:
                    token = Token.objects.filter(
                        security_key=request.REQUEST["token_"])[0]
                except:
                    raise dec.PermissionException("Bad Token")
                self.token = token
                self.ordered_by = token.issued_by

                output.update({'credit': self.token.remaining_credit})

                # FIXME: <<< EXACT BILLING SHOULD BE DONE AT THE END AND SHOULD BE PART OF JOBSCHEDULER
                # THIS SHOULD JUST BE VALIDATION
                # and self.ordered_by.get_account().credit > 0:
                if self.token.remaining_credit:
                    # atomic_incr(self.ordered_by.get_account(),'credit',-1)
                    atomic_incr(self.token, 'remaining_credit', -1)
                else:
                    return output
            else:
                raise dec.PermissionException("Bad Token")
            
            ##
            ##  Then let's resolve the algorithm
            ##
            from science.models import Algorithm
            if self.algorithm is None:
                try:
                    algorithm_id = request.POST.get(
                        "algorithm",
                        request.GET.get(
                            "algorithm",
                            None))
                    if isinstance(algorithm_id, list):
                        algorithm_id = algorithm_id[0]
                    algo = Algorithm.objects.get(id=algorithm_id)
                except Exception as e:
                    algo = None
                    print "bad algorithm ID ::", algorithm_id, e
                self.algorithm = algo

            if self.algorithm is None:
                return
            ##
            ##  AND CAPTURE THE BUCKET DATA
            ##                
            try:
                self._metadata = {
                    'bucketid': request.bucketid,
                    'bucketkey': request.bucketkey,
                    'buckethostid': request.buckethostid}
            except Exception as e:
                pass

            
            self.save()

            packageid = None
            
            # Generating a random access key for this result
            self.access_key = str(uuid.uuid1()) #<FIXME: CHECK uuid1/uuid4

            # Fields we want to get updated in request
            fields_update = {
                "query": {
                    "algorithm_id": self.algorithm.id,
                    "parameters": self.parameters},
                     "nrequestid": self}  # 'access_key':self.access_key}

            if (self.webhook):
                fields_update["on_complete"] = {"webhook": self.webhook}

            # Has a specific deployment been requested (assuming this is
            # allowed)
            if DEPLOYMENT_ORDERS_ENABLED and "server_id" in request.POST:
                import base64
                serv = base64.b64decode(request.REQUEST["server_id"])
                key = request.POST.get("server_key")
                number_of_deployed_servers = 1
                serverid = serv
            else:
                from compute.models import DeploymentRequest
                q = DeploymentRequest.objects.filter(
                    owner_id=self.ordered_by.id,
                    algorithm_id=self.algorithm.id)
                number_of_deployed_servers = q.count()
                if number_of_deployed_servers > 0:
                    serverid = str(q[0]["_id"])

            # adapt request
            if self.session is not None:
                fields_update.session = self.session.id

            from compute.lib import create_job, create_job_request

            with transaction.atomic():
                if number_of_deployed_servers > 0:  # I own servers
                    kwargs = {
                        "ulimits": {  # < LIMITS ON THESE SPECIFIC REQUEST (TODO: Implement)
                            "data": 0,
                            "memory": 0,
                            "cputime": 0,
                            "processes": 0
                        },
                        "platform_type": "linux_rack2014",
                        "nrequestid": self.id,
                        'package':self.algorithm.package,
                    }
                    fields_update.update(**kwargs)
                    job = create_job_request(
                        request,
                        "compute",
                        status="SCHEDULED",
                        use_server=serverid,

                        parameters=self.parameters,
                        return_object=True,
                        **fields_update)
                else:
                    # if you are not a server owner
                    # TODO : Check execute or compute ??? (use a reserved
                    # instance or a on-the-fly instance ??)
                    job = create_job(
                        request,
                        "/bin/sh","-c","./exe || ./exe.py || ./wio_exe",
                        package_id=self.algorithm.package_id,
                        status="SCHEDULED",
                        parameters=self.parameters,
                        return_object=True,
                        **fields_update)
                        
                # symlink the uploaded file to the right folder
                self.job = job
                self.status = STATUS_QUERY_SENT
                self.save()
                job.save()

            WITH_ZMQ=False
            if WITH_ZMQ:
              import zmq
              from wioframework.serializers.json import StartYnJSONEncoder
              zmq_context = get_zmq_context()
              port = 10000
              host = request.buckethostip  # < let's take the host of the bucket
              zsocket = zmq_context.socket(zmq.REQ)
              zsocket.setsockopt(zmq.SNDTIMEO, 500)
              zsocket.setsockopt(zmq.RCVTIMEO, 500)
              zsocket.connect("tcp://%s:%s" % (host, port,))
              zsocket.send(json.dumps(
                {'execrequest': self.__dict__, 'job': job.__dict__}, cls=StartYnJSONEncoder))
              zsocket.recv()
              zsocket.close()

            output.update({"access_key": self.access_key, "job_id": self.job.id})
            return output
        except Exception as ce:
            import backoffice.lib as debug
            debug.log_error(str(ce))
            import traceback
            import sys
            sys.stderr.write(
                "\n".join(map(str, traceback.format_tb(sys.exc_info()[2]))))
            print "EXCEPTION", ce
            return {'err': str(ce)}

    def on_expire(self):
        # FIXME: THE REQUEST FILES ARE NO MORE STORED HERE ....
        shutil.rmtree(
            "/wideio/requests/" +
            self.job.id +
            "/")  # delete all input and output files

    def can_view(self, request):
        return request.user.is_superuser or request.user == self.ordered_by

    def on_view(self, request):
        from cloud.models import Node
        if self.metadata and ("buckethostid" in self.metadata):
            proxy = Pyro4.Proxy(
                Node.objects.get(
                    id=self.metadata["buckethostid"]).metadata['ftd_uri'])
            proxy._pyroHmacKey = settings.HMACKEY
            dirlist = proxy.listdir(self.metadata["bucketid"])
            return {"files": dirlist,"status":self.status}
        return
        # DEPRECATED (KEPT FOR REFERENCE)
        if not request.user.is_superuser:
            if not self.access_key == request.REQUEST["access_key"]:
                raise dec.PermissionException("Access key does not match")
        try:
            r = mongo_db.queries.find(
                {'_id': ObjectId(self.job.id)})[0]  # < return the job
            if str(r["status"]) in ["DONE", "ERROR"]:
                if (self.status != STATUS_QUERY_REPLIED):
                    import backoffice.lib as debug
                    debug.log_info(
                        str(
                            "ALGORUNORDER %r job marked '%r' -> changing to status 2" %
                            (r["_id"], r["status"])))
                    self.status = STATUS_QUERY_REPLIED
                    self.save()
            else:
                import backoffice.lib as debug
                debug.log_info(str("ALGORUNORDER %r job marked '%r' : current status: %d" % (
                    r["_id"], r["status"], self.status)))

            # if self.status==STATUS_QUERY_SENT:
                #req_found = False
                #conn = SQSConnection(settings.AWS_ACCESS_ID, settings.AWS_SECRET_KEY)
                #compute_requests_queue = conn.create_queue('wideio_compute_replies')
                #msgs = compute_requests_queue.get_messages()
                # for i in range(0, len(msgs)):
                #msg_body = json.loads(msgs[i].get_body())
                # if msg_body["id"]==self.id:
                #self.results = self.results + msg_body["contents"]
                # compute_requests_queue.delete_message(msgs[i])
                # ~ i = i-1
                #req_found = True
                # if req_found == True:
                # self.status=STATUS_QUERY_REPLIED
            if str(r["status"]) in ["DONE", "ERROR"]:
                if settings.SSHFAST:
                    try:
                        import paramiko
                        sock = socket.socket(
                            socket.AF_INET,
                            socket.SOCK_STREAM)
                        sock.connect((settings.SSH_HOST, settings.SSH_PORT))
                        t = paramiko.Transport(sock)
                        t.start_client()

                        key = paramiko.DSSKey.from_private_key_file(
                            settings.SSH_DSS_KEY_FILE,
                            password=settings.SSH_KEY_PASSPHRASE)

                        t.auth_publickey(settings.SSH_USERNAME, key)

                        if not t.is_authenticated():
                            import backoffice.lib as debug
                            debug.log_info(str("ssh authentification failed"))
                            t.close()
                            return {}

                        sftp = paramiko.SFTPClient.from_transport(t)
                        # dirlist on remote host
                        dirlist = sftp.listdir(
                            "/wideio/requests/" +
                            self.job.id)
                        t.close()
                        return {"files": dirlist}

                    except Exception as ce:
                        import backoffice.lib as debug
                        import traceback
                        debug.log_info(traceback.format_exc())
                        return {}
                else:
                    return {
                        "files": os.listdir(
                            "/wideio/requests/" +
                            self.job.id)}
        except Exception as e:
            from backoffice.lib import log_error
            log_error("error while viewing request" + str(self.job.id))
            import traceback
            import sys
            sys.stderr.write(
                "\n".join(map(str, traceback.format_tb(sys.exc_info()[2]))))
            sys.stderr.write(e)
        return {}

    class WIDEIO_Meta:
        API_EXPORT_ALL = True
        NO_DRAFT = True
        icon = "icon-flag-alt"
        form_exclude = [
            'job_id',
            'ordered_by',
            'access_key',
            'token',
            'result',
            'use_server']
        # form_exclude += ['results','ipaddress']
        permissions = dec.perm_for_logged_users_only
        audlv_xargs = {
            'view_json_view': lambda x: dict(
                filter(
                    lambda i: i[0][0] != '_',
                    x.__dict__.items()))}
        audlv_xargs.update({'add_decorators': [csrf_exempt]})
        audlv_xargs.update({'view_decorators': [csrf_exempt]})

        
        
        #@wideio_relaction('ScientificModel',"Run a query",mimetype="text/html")
        # def run_a_query(self,possible=lambda u,r: (u.owner==r.user)):
        #    return HttpResponseRedirect(ExecRequest.get_add_url())

        class Actions:

            @wideio_action(
                possible=lambda self,
                request: self.ordered_by == request.user,
                icon="icon-reload")
            def restart_job(self, request):
                self.on_add(request)

        other_views = [
            {"name": "result",
             "doc": "Retrieve a file",
             "url": "compute/result/REQUESTID/",
             "inputs": [("access_key",
                         "You will need this access_key to retrieve the result",
                         "00000000-0000-0000-0000-000000000000"),
                        ("filename",
                         "the file you want to download1",
                         "image.png"),
                        ("job id", "the job ID", "ID")
                        ],
             "output": "0010101100... FILE"  # may also be a dict {key:value}
             }
        ]


# MODELS.append(ExecRequest)
# MODELS+=get_dependent_models(ExecRequest)

#    def testS3FS(self):
#        if not settings.PRODUCTION_MODE:
#            return True#
#
#        dirs = ["requests", "webdata", "datasets"]
#        dirs_down = []
#
#        for d in dirs:
#            if not os.path.isdir(os.path.join("/wideio", d)):
#                dirs_down.append(d)
#        if len(dirs_down) == 0:
#            return True
#        else:
#            for d in dirs_down:
#                devnull = open('/dev/null', 'w')
#                p = subprocess.Popen(
#                    ["/wideio/bin/wio_s3fs_restart", d], stdout=devnull, stderr=devnull)
#                p.communicate()
#
#            for i in range(0, 2):
#                time.sleep(3)#
#
#                still_down = []
#
#                for d in dirs_down:
#                    if not os.path.isdir(os.path.join("/wideio", d)):
#                        still_down.append(d)#
#
#                if len(still_down) == 0:
#                    return True
#            return False


            ###
            # OK THIS USED TO BE THE FILES ASSOCIATED WITH CURRENT REQUEST
            # MIMEJSON AND THE UPLOAD MANAGER SHALL NORMALLY HAVE ALREADY
            # UPLOADED THE DATA TO A RIGHT BUCKET SO THIS IS NO MORE NEEDED
            ###
            # if request.REQUEST.has_key("associated_files_"):
            #    for associated_file in request.REQUEST["associated_files_"].split(","):
            #        self.associated_files.add(associated_file)
            
            ###
            # HOUSEKEEPING RELATED TO THE FILES OF THE REQUEST (DEPRACATED ?)
            ###
            # self.requestid=str(x) #< FIXME: USE THE REAL REQUEST ID
            # os.mkdir("/wideio/requests/"+self.requestid+"/")
            # os.mkdir("/wideio/requests/"+self.requestid+"/output/")
            #id_list = [ ele["file_id"] for ele in mongo_db.compute_execrequest_associated_files.find({'execrequest_id' : ObjectId(self.id)})]
            # for associated_file in mongo_db.references_file.find({"_id":{"$in" :id_list}}):
                    # if settings.PRODUCTION_MODE:
                    # os.symlink(os.path.join("/wideio/webdata/"+str(associated_file["file"])),"/wideio/requests/"+self.requestid+"/"+str(associated_file["name"]))
                    # os.symlink(os.path.join("/wideio/webdata/"+str(associated_file["file"])),"/wideio/requests/"+self.requestid+"/image.png")
                    # else:
                    #os.symlink(os.path.join(settings.CWD, "media/"+str(associated_file["file"])),"/wideio/requests/"+self.requestid+"/"+str(associated_file["name"]))
                    ##os.symlink(os.path.join(settings.CWD, "media/"+str(associated_file["file"])),"/wideio/requests/"+self.requestid+"/image.png")
            #file("/wideio/requests/"+self.requestid+ "/request.bson","w").write(bson.BSON.encode((job)))
            # os.symlink("/wideio/packages/"+package+"/","models/current")
            # try:
                # os.chown("/wideio/requests/"+self.requestid+"/",os.stat("/wideio/requests/"+self.requestid+"/").st_uid,grp.getgrnam("wideio_slaves").gr_gid)  # << FIXME: "wideio_slaves
                # os.chmod("/wideio/requests/"+self.requestid+"/",0770) ## << FIXME: 770
            # except Exception,xe:
                #import backoffice.lib as debug
                # debug.log_error(str(xe))
                # print "exception while setting permissions for request folder",xe
                # return {'err':str(xe)}
            # OK EVERYTHING IS CONFIGURED - LET'S SAY TO EVERYONE THAT THE TASK IS SCHEDULE
            # x["_id"] on_complete={"dbupdate": (collection,  ( filteren,
            # {"$set": {} })):            